var map;
var snake = {
    0:{'direction':'left','x':500,'y':400},
    1:{'direction':'left','x':510,'y':400},
    2:{'direction':'left','x':520,'y':400}
};
var apple = {'x':0,'y':0};
var intervalInstance;

initSnake();
setApple();
startMove();
keyboardEvents();

function initSnake() {
    map = document.getElementById('map');

    for (tale in snake){
        map.innerHTML += '<div class="snake-tail" style="left:' + snake[tale]['x'] + 'px;top:' + snake[tale]['y'] + 'px;"></div>';
    }
    document.getElementsByClassName('snake-tail')[0].classList.add('snake-head');
}

function snakeMove() {
    var container = false;
    var tempContainer = false;
    for (tale in snake) {
        if (container) {
            tempContainer = container;
        }
        container = {'direction': snake[tale]['direction'], 'x': snake[tale]['x'], 'y': snake[tale]['y']};
        if (tempContainer) {
            snake[tale] = tempContainer;
        }

        if (tale == 0) {
            switch (snake[tale]['direction']) {
                case 'top':
                    snake[tale]['y'] -= 10;
                    break;
                case 'left':
                    snake[tale]['x'] -= 10;
                    break;
                case 'down':
                    snake[tale]['y'] += 10;
                    break;
                case 'right':
                    snake[tale]['x'] += 10;
                    break;
            }
            //apple
        }
        document.getElementsByClassName('snake-tail')[tale].style.left = snake[tale]['x'] + 'px';
        document.getElementsByClassName('snake-tail')[tale].style.top = snake[tale]['y'] + 'px';
    }
}
function startMove(){
    intervalInstance = setInterval(function () {
        snakeMove();
    }, 500);
}
function stopMove() {
    clearInterval(intervalInstance);
    intervalInstance = false;
}
function changeDirection(direction){
    snake[0]['direction'] = direction;
    stopMove();
    snakeMove();
    startMove();
}

function setApple(){
    map.innerHTML += '<div class="apple" style="left:' + random('x', 1, map.offsetWidth) + 'px;top:' + random('y', 1, map.offsetHeight) + 'px;"> 🍎 </div>';
}
function random(coord, min, max) {
    var rand = false;
    while (rand === false){
        rand = Math.floor(Math.random() * (max - min) + min);
        for (tale in snake) {

        }
    }
    return rand;
}

function keyboardEvents() {
    document.addEventListener('keydown',function (e) {
        console.log(e.keyCode);
        switch (e.keyCode){
            case 32:
                intervalInstance ? stopMove() : startMove();
                break;
            case 38: case 87:
                changeDirection('top');
                break;
            case 37: case 65:
                changeDirection('left');
                break;
            case 40: case 83:
                changeDirection('down');
                break;
            case 39: case 68:
                changeDirection('right');
                break;
            default:
                //@todo: Help notice
        }
    });
}